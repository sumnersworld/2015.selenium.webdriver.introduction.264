﻿using NUnit.Framework;

namespace Selenium.WebDriver.Introduction
{
    //Note the Tag implementation here.  This is to indicate
    //that these are test project parameters and not test
    //ones.  Think of this as a test suite setup file.
    [SetUpFixture]
    public class Test_Configuration
    {
        //This is what is required to setup before starting
        //the individual test files.  This could include the
        //command to visit the site if you never plan to change
        //however its common to leave that in the test so that you
        //always start the test at the starting point.
        [SetUp]
        public void Setup()
        {
        }
        //This is the last item to complete once all tests are completed
        //this differs from the TestFixtureTearDown in that those run at
        //the end of each test, these are at the end of all tests.  The
        //driver.Quit is moved to here, as we wish to reuse the driver
        //during multiple test setups.
        [TearDown]
        public void TearDown()
        {
            if (seleniumDriver.Driver != null) seleniumDriver.Driver.Quit();
            seleniumDriver.isDriverRunning = false;
        }
    }
}