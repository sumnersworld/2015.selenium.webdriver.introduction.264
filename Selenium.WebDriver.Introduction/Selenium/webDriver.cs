﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using Selenium.WebDriver.Introduction.Properties;

namespace Selenium.WebDriver.Introduction
{
    public class seleniumDriver
    {
        public seleniumDriver()
        {
            isDriverRunning = false;
        }
        internal static bool isDriverRunning;
        private static IWebDriver _driver;
        public static IWebDriver Driver
        {
            get
            {
                if (_driver == null)
                {
                    if (!isDriverRunning)
                    {
                        var browserName = Settings.Default.browser;
                        if (!String.IsNullOrEmpty(browserName))
                        {
                            switch (browserName)
                            {
                                case "Chrome":
                                    _driver = new ChromeDriver();
                                    ConfigureDriver();
                                    break;
                                case "Firefox":
                                    _driver = new FirefoxDriver();
                                    ConfigureDriver();
                                    break;
                                case "IE":
                                    _driver = new InternetExplorerDriver();
                                    ConfigureDriver();
                                    break;
                                default:
                                    Console.WriteLine("App.config key error.");
                                    Console.WriteLine("Defaulting to Firefox");
                                    _driver = new FirefoxDriver();
                                    ConfigureDriver();
                                    break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("* * * * DEFAULTMODE * * * *");
                            Console.WriteLine("App.config key not present.");
                            _driver = new FirefoxDriver();
                            ConfigureDriver();
                        }
                    }
                    isDriverRunning = true;
                }
                return _driver;
            }
        }
        internal static void ConfigureDriver()
        {
            _driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 30));
            _driver.Manage().Cookies.DeleteAllCookies();
            _driver.Manage().Window.Maximize();
            isDriverRunning = true;
        }
    }
}