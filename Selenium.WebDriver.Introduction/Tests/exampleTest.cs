﻿using NUnit.Framework;

namespace Selenium.WebDriver.Introduction
{
    [TestFixture]
    // The Class name here always reflects what is shown in the results
    // and as such should be named appropriately
    public class GoogleTest
    {
        //The setup is used to make preparations before the test begins
        //this instance includes a visit to the base page as we may
        //decide to switch sites between tests, or at least reset the
        //same site to ensure the tests are performed from the same
        //starting point.
        [SetUp]
        public void SetUp()
        {
            seleniumDriver.Driver.Navigate().GoToUrl("http://www.google.co.uk");
        }
        [Test]
        //As shown in previous examples the method for the test is what
        //is displayed in the test results, so as with the class name
        //this should hold a naming convention that is appropriate to
        //the testing being performed.
        public void GooglePageTitle()
        {
            Assert.AreEqual("Google", seleniumDriver.Driver.Title);
        }
    }
}